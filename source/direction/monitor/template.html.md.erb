---
layout: markdown_page
title: "Product Vision - Monitor"
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Monitor. If you'd like to discuss this vision
directly with the product manager for Monitor, feel free to reach out to Joshua
Lambert via [e-mail](mailto:joshua@gitlab.com) or by [scheduling a video call](https://calendly.com/joshua-lambert/product-chat/).

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amonitor&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

## Overview

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/kewoKxbvSJI" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
</figure>

Performance is a critical aspect of the user experience, and ensuring your
application is responsive and available is everyone's responsibility. We want to
help address this need for development teams, by integrating key performance
analytics and feedback into the tool developers already use every day.

As part of our commitment to performance we are also deeply instrumenting GitLab
itself, enabling our team to improve GitLab peformance and for customers to more
easily manage their deployments.

<%= partial("direction/categories", :locals => { :stageKey => "monitor" }) %>

## New super powers for 2019

GitLab is a unique product, a single application which spans the entire devops
lifecycle. This breadth offers both the capability for a unified user
experience, as well as emergent capabilities.

### Accelerate time to resolve

We want to help teams resolve outages faster, accelerating both the
troubleshooting and resolution of incidents. GitLab's single platform can
analyze the incoming observability data with known CI/CD events and source code
infomation, to automatically suggest potential root causes. GitLab's Web IDE can
also provide an optimized coding experience, presenting the source code enriched
with related observability data like stack traces.

<figure class="video_container">
   <iframe width="560" height="315" src="https://www.youtube.com/embed/ZgFqyXCsqPY?start=4120" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

* Detect errors that are occuring via [Tracing](https://gitlab.com/groups/gitlab-org/-/epics/89),
  [Error Tracking](https://gitlab.com/groups/gitlab-org/-/epics/169) and [Metrics](https://gitlab.com/groups/gitlab-org/-/epics/190)
* First class [Incidents](https://gitlab.com/groups/gitlab-org/-/epics/349),
  providing a single source of truth across the whole lifecycle for improved
  collaboration and communication.
* Automatic correlatation of [errors](https://gitlab.com/groups/gitlab-org/-/epics/419),
  traces, [deployments](https://gitlab.com/groups/gitlab-org/-/epics/420), and [feature flags](https://gitlab.com/groups/gitlab-org/-/epics/421)
  with [incidents](https://gitlab.com/groups/gitlab-org/-/epics/349)
* Enrich Web IDE with [error](https://gitlab.com/groups/gitlab-org/-/epics/426)
  and [trace](https://gitlab.com/groups/gitlab-org/-/epics/427) data
* [Production testing](https://gitlab.com/groups/gitlab-org/-/epics/168) to help
  detect problems before users do

### Balancing velocity and stability

There is often a tension between the velocity of shipping new features and
stability of the overall service. The desired balance is frequently different
between companies, as well as between services within a company.

GitLab can help companies optimize this balance, by comparing [service level objectives](https://gitlab.com/groups/gitlab-org/-/epics/432)
with the velocity measured by [value stream analytics](https://gitlab.com/groups/gitlab-org/-/epics/215).

* Set [SLO's and SLA's](https://gitlab.com/groups/gitlab-org/-/epics/432) for
  key metrics
* Track historical [story point velocity](https://gitlab.com/groups/gitlab-org/-/epics/435)
* View an [executive dashboard](https://gitlab.com/groups/gitlab-org/-/epics/436)
  displaying SLO's, development velocity, contributor counts, KPI's
* Ensure issues that were created to prevent repeat incidents are
  [actually scheduled and delivered](https://gitlab.com/gitlab-org/gitlab-ee/issues/7821)

### Drive operational efficiency

[Control cloud spend, optimize compute.](https://gitlab.com/groups/gitlab-org/-/epics/503)

* Cost estimator for Kubernetes clusters
* Tie utilization of shared resources to specific projects
* Flag wasteful resources (requests >>> utilization)
* Optimize node selection
* Automatically suggest appropriate resource requests based on observed behavior

## Functional areas

### Tracing

* Trace comparison tool (good/bad), vs statistical model
* Build statistical model of categories of traces (successful, failure)
* Aggregate trace failures against code sections, detect new hotspots 
* Embeddable trace views

### Logging

* Integrate Elastic Search with GitLab
* Build structured log viewer/search interface
* Embeddable log views
* Log alerts

### Error tracking

* [Integrate with Sentry](https://gitlab.com/groups/gitlab-org/-/epics/169)

### Application Monitoring

* [Custom dashboards](https://gitlab.com/gitlab-org/gitlab-ee/issues/6019)
* Embeddable metrics charts
* [Anomaly and outlier alerts](https://gitlab.com/groups/gitlab-org/-/epics/119)

### Infrastructure Monitoring
* [Cluster monitoring improvements](https://gitlab.com/groups/gitlab-org/-/epics/140)
* [Cluster Ops dashboard](https://gitlab.com/groups/gitlab-org/-/epics/192)
* [Cluster cost optimization](https://gitlab.com/groups/gitlab-org/-/epics/503)

### Service Mesh Monitoring

* [Istio integration](https://gitlab.com/groups/gitlab-org/-/epics/342)
* [Service Map](https://gitlab.com/groups/gitlab-org/-/epics/342) 

### Incident management

* [Centralized alerting framework](https://gitlab.com/gitlab-org/gitlab-ee/issues/3627)
* [Service Status Page](https://gitlab.com/gitlab-org/gitlab-ee/issues/3557)
* Pagerduty integration

### GitLab Instance Monitoring

* [GitLab Health Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/303)

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/#prioritization)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

You can see our entire public backlog for Monitor at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Monitoring);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the sectiond below.

## Direction

<%= direction %>

<%= partial("direction/other", :locals => { :stage => "monitor" }) %>
