---
layout: job_family_page
title: "Evangelist program manager"
---

As the Evangelist Program Manager, you will be responsible for enabling members from the GitLab community (mostly not GitLab team members) to evangelize GitLab.

## Responsibilities

* Help people write blog posts about GitLab. Curate topics, introduce people to experts, cross-post to our blog, give incentives. Result is the number of blog posts and views per month.
* Help people give talks about GitLab. Pair people with requests from event organizers, sponsor events, ensure their travel is paid for, introduce them to experts in the company. Result is the number of talks and audience members per month.
* Help people organize meetups about GitLab. Have a starter package, surface interesting content, pair them with experts, pay for location and food costs. Result is the number of meetups and meetup visitors per month.

## Requirements

* You have 5-7 years of experience running developer relations or community advocacy programs, preferably open source in nature.
* Analytical and data driven in your approach to building and nurturing communities.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* Excellent spoken and written English.
* Familiarity with developer tools, Git, Continuous Integration, Containers, and Kubernetes.
* Experience in communicating with writers on a range of technical topics is a plus.
* You share our [values](/handbook/values/), and work in accordance with those values.
