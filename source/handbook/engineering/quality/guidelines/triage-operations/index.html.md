---
layout: markdown_page
title: "Triage Operations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Any GitLab team member can triage issues. Keeping the number of un-triaged issues low is essential for maintainability, and is our collective responsibility.

We have implemented automation and tooling to handle this at scale and distribute the load to each team or group.

## Triage Packages

A [triage package](https://gitlab.com/gitlab-org/quality/team-tasks/issues/35) is an issue containing a checklist of issues requiring attention. 
Each task corresponds to an issue that needs labels, prioritization and/or scheduling.

### Current Packages

#### Unlabelled issues

This package contains the recent unlabelled issues.

  * Triage owner: Quality Department Engineers.
  * Triage action: 
    1. Close the issue if it is no longer relevant or a duplicate. Check for duplicates in this project and others (it is common for issues reported in EE to already exist in CE)
    1. Add a [type label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#type-labels)
    1. Add a [team label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#team-labels)
    1. Add relevant [subject labels](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#subject-labels)
    1. [Mention relevant domain experts](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#issue-triaging) if the issue requires further attention
  * Example: [https://gitlab.com/gitlab-org/gitlab-ce/issues/57834](https://gitlab.com/gitlab-org/gitlab-ce/issues/57834)

#### Unscheduled stale issues

This package contains issues that have not been scheduled to work on and have been inactive for a long time.

  We currently detect them at 3 months and 6 months. One package for each.
  * Triage owner: Quality Department Engineers.
  * Triage action: 
    1. Close the issue if it is no longer relevant or a duplicate.
    1. If still relevant, ensure that the correct labels are added.
    1. [Mention relevant domain experts](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#issue-triaging).
  * Example: [https://gitlab.com/gitlab-org/gitlab-ce/issues/57513](https://gitlab.com/gitlab-org/gitlab-ce/issues/57513)

#### Group level issues

This package contains the relevant bugs and feature requests that belong to a group e.g. Create, Plan, Manage and etc.

The package itself is divided into 3 parts. The first part containing feature proposals, second frontend bugs and lastly general bugs (likely backend).

* Example: [https://gitlab.com/gitlab-org/quality/triage-ops/issues/118](https://gitlab.com/gitlab-org/quality/triage-ops/issues/118)

##### Feature proposals

This section contains issues with the `~feature` label without a milestone.

  * Triage owner: Product Manager(s) for that group.
  * Triage actions:
    1. If the issue is a duplicate or irrelevant, close the issue out.
    1. Assign a milestone either to a versioned milestone, `Backlog` or `Awaiting further demand` milestone.

##### Frontend bugs

This section contains issues with the `~bug` and `~frontend` label without priority and severity.

  * Triage owner: Frontend Engineering Manager(s) for that group.
  * Triage actions:
    1. Close the issue if it is no longer relevant or a duplicate.
    1. Assign a [Priority Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#priority-labels).
    1. Assign a [Severity Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).
    1. Assign either a versioned milestone or to the `Backlog`.

##### Bugs (likely backend)

This section contains issues with the `~bug` label without priority and severity.

  * Triage owner: Backend Engineering Manager(s) for that group.
  * Triage actions:
    1. Close the issue if it is no longer relevant or a duplicate.
    1. Assign a [Priority Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#priority-labels).
    1. Assign a [Severity Label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).
    1. Assign either a versioned milestone or to the `Backlog`.  

#### Support issues

This package contains the issues with the label `~support request` that have been inactive for 3 or more months.

  * Triage owner: Quality Department Engineers.
  * Triage action: 
    1. Route issue creator to https://about.gitlab.com/support/
  * Example: [https://gitlab.com/gitlab-org/gitlab-ce/issues/57846](https://gitlab.com/gitlab-org/gitlab-ce/issues/57846)

#### Community Contributions

This package contains open merge requests which has been submitted by non-gitlab employees.

These merge requests would have the `~Community Contribution` label

  * Triage owner: [@gitlab-org/coaches](https://gitlab.com/gitlab-org/coaches).
  * Triage action: 
    1. Determine if the merge request should be followed through or closed.
    1. Determine if the merge request is ready or further changes are required.
    1. Assign a reviewer as needed. 
  * Example: [https://gitlab.com/gitlab-org/gitlab-ce/issues/58131](https://gitlab.com/gitlab-org/gitlab-ce/issues/58131)
  
## Resources

* [Issue Triage Policies](/handbook/engineering/issue-triage/).
* Chat channels; we use our chat internally as a realtime communication tool:
  * [#triage](https://gitlab.slack.com/messages/triage): general triage team channel.
  * [#gitlab-issue-feed](https://gitlab.slack.com/messages/gitlab-issue-feed) - Feed of all GitLab-CE issues
  * [#support-tracker-feed](https://gitlab.slack.com/messages/support-tracker-feed) - Feed of the GitLab.com Support Tracker
  * [#mr-coaching](https://gitlab.slack.com/messages/mr-coaching): for general conversation about Merge Request coaching.
  * [#opensource](https://gitlab.slack.com/messages/opensource): for general conversation about Open Source.
