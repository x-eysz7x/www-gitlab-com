---
layout: markdown_page
title: "Open source Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

- GitLab's top tiers (self-hosted Ultimate and cloud-hosted Gold) are [free for OSS projects](/solutions/open-source/)
- GitLab OSS program [repository](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/)

## Requirements

- Any project that uses an OSI-approved open source license and which does not seek to make a profit from the resulting project software may apply.
- See our [full terms](/terms/#edu-oss) regarding this program
- For more questions, please see the [FAQ section](/solutions/open-source/#FAQ)

## Workflows

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-opensource/)

## Metrics

SFDC Opportunities by Stage for OSS campaign - SFDC [report](https://na34.salesforce.com/00O61000004hfom)